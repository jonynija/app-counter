import React, {useState} from 'react';
import {Text, View} from 'react-native';
import {Button, FAB} from 'react-native-paper';
import {appStyles} from '../theme/app.styles';
import Icon from 'react-native-vector-icons/MaterialIcons';

export const CounterM3Screen = () => {
  const [count, setCount] = useState(1);

  return (
    <View style={appStyles.centerContainer}>
      <Text style={appStyles.title}>{count}</Text>

      <Icon name="add" size={50} />

      <FAB
        icon="add"
        style={appStyles.fab}
        onPress={() => {
          setCount(count + 1);
        }}
        onLongPress={() => {
          setCount(0);
        }}
      />
    </View>
  );
};
